%% Main file for running non-linear heat conduction problems using 
%% Bubnov-Galerkin FEM

% Clear workspace variables and close existing figures 
clear ;
close all ;

%% PRE-PROCESSING

% Symbolic variable representing temperature
syms th ;
syms xpos ;
% Thermal conductivity as a function of temperature 
kfunsym = 4 + sin(th) + exp(th) + log(th) ;
qfunsym = xpos + 5 ;

% Domain size 
l = 1 ;

% Number of elements
nels = 8 ;

% Dirichlet BCs
% Temperatures specified at the left and right end respectively 
tl = 1 ;
tr = 3 ;

% Tolerance for Newton-iteration convergence
toler = 1.0e-6 ;

% Element order
ordr = 2 ;

%% SOLVE
% Function returns a structure containing the following information
% variables

% trun - Time consumed to solve the problem
% temp - Temperature values at the nodes 
% res - Error measure 
% niter - Number of Newton-Raphson iterations to convergence
% xsp - Nodal coordinate vector 


s = nlht_dbc_ho(kfunsym,qfunsym,l,nels,tl,tr,toler,ordr) ;
%s = nlht_dbc_lin(kfunsym,qfunsym,l,nels,tl,tr,toler) ;
% Plot approximate solution
figure ;
hold on ;
h1 = scatter(s.xsp,s.temp,15,'b') ;
f1 = plot(s.xsp,s.temp,'r') ;

% %% ANALYTICAL SOLUTION
% % Compute integral of thermal conductivity for analytical solution
% kint = int(kfunsym,th) ;
% kfunint = matlabFunction(kint,'Vars',th) ;
% 
% % Constants of integration in analytical solution
% dpar = kfunint(tl) ;
% cpar = (kfunint(tr) - kfunint(tl))/l ;
% fnew = (kint - dpar)/cpar ;
% fnew = matlabFunction(fnew,'Vars',th) ;
% tpts = linspace(tl,tr,250) ;
% xpts2 = fnew(tpts) ; 
% 
% % Plot analytical solution
% h2 = plot(xpts2,tpts,'g') ;

% Plot decorations
grid on ;
grid minor ;
xlabel('$x \rightarrow$','interpreter','latex') ;
ylabel('$T \rightarrow$','interpreter','latex') ;
title('Temperature distribution') ;
%legend({'FEM approximation','Analytical solution'},'Location','southeast') ;
axis tight ;

