function s = nlht_dbc_lin(kfunsym,qfunsym,l,nels,tl,tr,tolr) 
%% FEM code for non-linear heat conduction in 1D
% Author : Aaditya Lakshmanan
% The code computes the Finite-Element estimate of the solution to the non-linear heat
% equation d/dx(K(T) dT/dx) = 0 with Dirichlet boundary conditions.
% Newton-iteration is employed along with linear elements and residual
% computation using the finite element approximation of the L2 norm. The
% stiffness matrix from previous iteration is used as the Jacobian for the
% current Newton iteration. 


% Linear basis functions parametrized w.r.t isoparametric coordinates

intfuns ={@(psi) (1-psi)/2;@(psi) (1+psi)/2} ;
intfunsder ={@(psi) -1/2;@(psi) 1/2} ;
% Derivatives of basis functions w.r.t isoparametric coordinates

% Thermal conductivity as function of temperature
syms th ;
syms xpos ;

kfunsym = subs(kfunsym,th) ;
qfunsym = subs(qfunsym,xpos) ;

% Convert the thermal conductivity, its derivative and integral to
% anonymous function
kfun = matlabFunction(kfunsym,'Vars',th) ;
kfunt = matlabFunction(diff(kfunsym,th),'Vars',th) ;
qfun = matlabFunction(qfunsym,'Vars',xpos) ;

% Mesh information - number of elements, element length, number of degrees
% of freedom
le = l/nels ;
nds = nels + 1 ;

% Gauss-Legendre quadrature info - Points and weights
[pt,wt] = lgwt(1,-1,1) ;

% Basis function evaluation at Gauss points
ifval(1,1) = intfuns{1}(pt) ;
ifval(2,1) = intfuns{2}(pt) ;

% Basis function derivative evaluation at Gauss points
ifderval(1,1) = intfunsder{1}(pt) ;
ifderval(2,1) = intfunsder{2}(pt) ;

% Domain discretization
xpts = linspace(0,l,nds)' ;

% Compute body force vector 
bfrc = zeros(nds,1) ;
for i=1:nels 
    bfrc(i,1) = bfrc(i,1) + le/2*qfun((xpts(i,1) + xpts(i+1,1) )/2)*wt*ifval(1,1)  ;
    bfrc(i+1,1) = bfrc(i+1,1) + le/2*qfun((xpts(i,1) + xpts(i+1,1))/2)*wt*ifval(2,1) ;
end



% Starting guess for temperature field
tst = linspace(tl,tr,nds)' ;

% Residual
res = 1 ;


% Initialize Jacobian for Newton iteration
kall = zeros(nds,nds) ;

% Iteration counter
niter = 0 ;

% Loop until residual within tolerance or number of iterations crosses a
% specified maximum value 

% Start timer
tic ;

while(res > tolr && niter<2000)
    kall = zeros(nds,nds) ;
    % Construct Stiffness matrix
    for i=1:nels
        % Elemental stiffness matrix
         kel1 = ifderval*ifderval' ;
         kmul = tst(i:i+1,1)'*ifval ;
         kmul = kfun(kmul) ;
         kel1 = 2/le*wt*kmul*kel1 ;  
         
        % Assemble elemental into global Jacobian matrix  
         kall(i:i+1,i:i+1) = kall(i:i+1,i:i+1) + kel1  ; 
           
    end
  
    % Residual vector 
    rhsvec = (-kall*tst) + bfrc ;
    rhsvec = rhsvec(2:end-1,1) ;
  
       % Second contribution arising from linearization of thermal
       % conducitivity as a functio nof temperature
   for i=1:nds
       if(i==1)
           
         kel1 = ifderval*ifderval' ;
         
         kmul = tst(i:i+1,1)'*ifval ;
        
         kmul = kfunt(kmul) ;
        
         kel1 = 2/le*wt*kmul*kel1 ;
        
         kel1 = kel1*intfuns{1}(pt) ;
          
         coldat = kel1*tst(i:i+1,1) ;
         kall(i:i+1,i) = kall(i:i+1,i) + coldat(1:2,1) ;
       
         
         elseif(i==nds)
         kel1 = ifderval*ifderval' ;
         kmul = tst(i-1:i,1)'*ifval ;
         kmul = kfunt(kmul) ;
         kel1 = 2/le*wt*kmul*kel1 ;
         kel1 = kel1*intfuns{2}(pt) ;
         coldat = kel1*tst(i-1:i,1) ;
         kall(i-1:i,i) = kall(i-1:i,i) + coldat(1:2,1) ;
          
      elseif(i>1 && i<nds)
           
          kels = zeros(3) ;
         kel1 = ifderval*ifderval' ;
         kmul = tst(i-1:i,1)'*ifval ;
         kmul = kfunt(kmul) ;
         kel1 = 2/le*wt*kmul*kel1 ;
         kel1 = kel1*intfuns{2}(pt) ;
         
         kel2 = ifderval*ifderval' ;
         kmul = tst(i:i+1,1)'*ifval ;
         kmul = kfunt(kmul) ;
         kel2 = 2/le*wt*kmul*kel2 ;
         kel2 = kel2*intfuns{1}(pt) ;
         
         kels(1:2,1:2) = kels(1:2,1:2) + kel1 ;
         kels(2:3,2:3) = kels(2:3,2:3) + kel2 ;
         coldat = kels*tst(i-1:i+1,1) ;
         kall(i-1:i+1,i) = kall(i-1:i+1,i) + coldat(1:3,1) ;
         
         
       end
      
   end
 
  
    % Row and column deletion based on BCs
    ksm = kall(2:end-1,2:end-1) ;
    
    % Compute correction to temperature from previous iteration
    delta_t = ksm\rhsvec ;   
    delta_t_b = [0;delta_t;0] ;
    
    % Update temperature estimate
    tst(2:end-1,1) = tst(2:end-1,1) + delta_t ;
    
    % Compute L2 norm of temperature increment using FE basis functions to
    % estimate the integral
    res = 0 ;
    for i=1:nels
    res = res + (delta_t_b(i:i+1,1)'*ifval)^2*wt ;
    end
    res = res*le/(2*l) ;
    
    % Increment iteration counter
   niter = niter + 1 ;
end  

% End timer

% Define structure
s.trun = toc ;
s.temp = tst ;
s.res = res ;
s.niter = niter ;
s.xsp = xpts ;
 
end


