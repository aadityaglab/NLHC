function s = nlht_dbc_ho(kfunsym,qfunsym,l,nels,tl,tr,tolr,ordr) 
%% FEM code for non-linear heat conduction in 1D
% With higher order elements 

% Order of polynomial function used for interpolation
nnes = ordr + 1 ;
intvals = eye(nnes) ;
dmn = linspace(-1,1,nnes) ;
% Basis functions 
syms x ;
for i=1:nnes
bsfunsym(i,1) = coefficient(dmn,intvals(i,:),i) ;
bsfunsym(i,1) = subs(bsfunsym(i,1),x) ;
bsfundersym(i,1) = diff(bsfunsym(i,1),x) ;
bsfunano{i} = matlabFunction(bsfunsym(i,1),'Vars',x) ;
bsfunderano{i} = matlabFunction(bsfundersym(i,1),'Vars',x) ;
end

% Define thermal conductivity as function of temperature
syms th xpos;
kfunsym = subs(kfunsym,th) ;
qfunsym = subs(qfunsym,xpos) ;
qfun = matlabFunction(qfunsym,'Vars',xpos) ;

% Convert the thermal conductivity, its derivative and integral to
% anonymous function
kfun = matlabFunction(kfunsym,'Vars',th) ;
kfunt = matlabFunction(diff(kfunsym,th),'Vars',th) ;

% Mesh info
le = l/nels ;
nds = ordr*nels + 1 ;

% Gauss-Legendre quadrature info - Points and weights
[pt,wt] = lgwt(ordr,-1,1) ;

% Function evaluation at Gauss points
for i=1:nnes
ifval(i,:) = bsfunano{i}(pt) ;
ifderval(i,:) = bsfunderano{i}(pt) ;
end 

% Domain discretization
xpts = linspace(0,l,nds)' ;

% Compute body force vector 
bfrc = zeros(nds,1) ;
for i=1:nels 
   for j=1:ordr
       xinterp = xpts((i-1)*ordr+1:i*ordr+1,1)'*ifval(:,j) ;
       bfrc((i-1)*ordr+1:i*ordr+1,1) = bfrc((i-1)*ordr+1:i*ordr+1,1) + ifval(:,j)*le/2*wt(j,1)*qfun(xinterp) ; 
   end
end


% Starting guess for temperature field
tst = linspace(tl,tr,nds)' ;

% Residual
res = 1 ;

% Jacobian for Newton iteration
kall = zeros(nds,nds) ;

% Iteration counter
niter = 0 ;

% Loop until residual within tolerance or number of iterations crosses a
% specified maximum value 
tic ;
while(res > tolr && niter<1500)
    kall = zeros(nds,nds) ;
    % Construct Jacobian
    for i=1:nels
        % Elemental Jacobian matrix - Contribution 1
        kel1 = zeros(nnes,nnes) ;
        for j=1:ordr
         keltemp = ifderval(:,j)*ifderval(:,j)' ;
         kmul = tst(ordr*(i-1)+1:ordr*(i-1)+nnes,1)'*ifval(:,j) ;
         kmul = kfun(kmul) ;
         kel1 = kel1 + 2/le*wt(j,1)*kmul*keltemp ;  
        end  
       % Assemble into global Jacobian matrix  
         kall(ordr*(i-1)+1:ordr*(i-1)+nnes,ordr*(i-1)+1:ordr*(i-1)+nnes) = kall(ordr*(i-1)+1:ordr*(i-1)+nnes,ordr*(i-1)+1:ordr*(i-1)+nnes) + kel1 ;
           
    end
    
    % Residual vector 
    rhsvec = (-kall*tst) + bfrc ;
    rhsvec = rhsvec(2:end-1,1) ;
    
    % Contribution 2 
    for i=1:nds
       if(mod(i,ordr) ~= 1 && ordr ~= 1)
        
         for j=1:ordr
         kel1 = ifderval(:,j)*ifderval(:,j)' ;
         kmul = tst((ceil(i/ordr)-1)*ordr+1:(ceil(i/ordr))*ordr+1,1)'*ifval(:,j) ;
                
         kmul = kfunt(kmul) ;
         kel1 = 2/le*wt(j,1)*kmul*kel1 ;
         kel1 = kel1*bsfunano{mod(i-1,ordr)+1}(pt(j,1)) ;
         coldat = kel1*tst((ceil(i/ordr)-1)*ordr+1:(ceil(i/ordr))*ordr+1,1) ;
         kall((ceil(i/ordr)-1)*ordr+1:(ceil(i/ordr))*ordr+1,i) = kall((ceil(i/ordr)-1)*ordr+1:(ceil(i/ordr))*ordr+1,i) + coldat(1:ordr+1,1) ;
         end
         
         
         
       elseif(i==1)
           
          for j=1:ordr
         kel1 = ifderval(:,j)*ifderval(:,j)' ;
         
         kmul = tst(1:ordr+1,1)'*ifval(:,j) ;
        
         kmul = kfunt(kmul) ;
          
         kel1 = 2/le*wt(j,1)*kmul*kel1 ;
         
         kel1 = kel1*bsfunano{1}(pt(j,1)) ;
         
         coldat = kel1*tst(1:ordr+1,1) ;
         kall(1:ordr+1,i) = kall(1:ordr+1,i) + coldat(1:ordr+1,1) ;
          end
           
        
          
       elseif(i==nds)
        
           for j=1:ordr
         kel1 = ifderval(:,j)*ifderval(:,j)' ;
         kmul = tst(nds-ordr:nds,1)'*ifval(:,j) ;
         kmul = kfunt(kmul) ;
         kel1 = 2/le*wt(j,1)*kmul*kel1 ;
         kel1 = kel1*bsfunano{ordr+1}(pt(j,1)) ;
         coldat = kel1*tst(nds-ordr:nds,1) ;
         kall(nds-ordr:nds,i) = kall(nds-ordr:nds,i) + coldat(1:ordr+1,1) ;
           end
           
       else
        
           for j=1:ordr
         kels = zeros(2*ordr+1) ;
         
         kel1 = ifderval(:,j)*ifderval(:,j)' ;
         
         kmul = tst((ceil(i/ordr)-2)*ordr+1:(ceil(i/ordr)-1)*ordr+1,1)'*ifval(:,j) ;
         
         kmul = kfunt(kmul) ;
         kel1 = 2/le*wt(j,1)*kmul*kel1 ;
         kel1 = kel1*bsfunano{ordr+1}(pt(j,1)) ;
         
         kel2 = ifderval(:,j)*ifderval(:,j)' ;
         kmul = tst((ceil(i/ordr)-1)*ordr+1:(ceil(i/ordr))*ordr+1,1)'*ifval(:,j) ;
         kmul = kfunt(kmul) ;
         kel2 = 2/le*wt(j,1)*kmul*kel2 ;
         kel2 = kel2*bsfunano{1}(pt(j,1)) ;
         
         kels(1:(ordr+1),1:(ordr+1)) = kels(1:(ordr+1),1:(ordr+1)) + kel1 ;
         kels((ordr+1):2*ordr+1,(ordr+1):2*ordr+1) = kels((ordr+1):2*ordr+1,(ordr+1):2*ordr+1) + kel2 ;
         
         coldat = kels*tst((ceil(i/ordr)-2)*ordr+1:(ceil(i/ordr))*ordr+1,1) ;
         kall((ceil(i/ordr)-2)*ordr+1:(ceil(i/ordr))*ordr+1,i) = kall((ceil(i/ordr)-2)*ordr+1:(ceil(i/ordr))*ordr+1,i) + coldat(1:2*ordr+1,1) ;
           end
           
       end
      
    end

  
 
    % Row and column deletion based on BCs
    ksm = kall(2:end-1,2:end-1) ;
    delta_t = ksm\rhsvec ;   
    delta_t_b = [0;delta_t;0] ;
    % Update temperature estimate
    tst(2:end-1,1) = tst(2:end-1,1) + delta_t ;
    
    % Compute L2 norm of temperature increment using FE basis functions to
    % estimate the integral
    res = 0 ;
    for i=1:nels
        for j=1:ordr
    res = res + (delta_t_b((i-1)*ordr+1:ordr*i+1,1)'*ifval(:,j))^2*wt(j,1) ;
        end
    end
    res = res*le/(2*l) ;
    
    % Increment iteration counter
   niter = niter + 1 ;
   
end    

% Define structure
s.trun = toc ;
s.temp = tst ;
s.res = res ;
s.niter = niter ;
s.xsp = xpts ;


end



