# HEAT CONDUCTION PROBLEM - FEM 

## Non-linear heat conduction implementation using **Bubnov-Galerkin** FEM. Versatile to include the following :

1. *Arbitrary body heat generation functions - Done*
2. *Arbitrary thermal conductivity temperature functions - Done*
3. *Dirichlet BCs - Done*
4. *Mesh refinement, element order - Done*
5. *Multiple error measures*


